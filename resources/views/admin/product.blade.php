<!DOCTYPE html>
<html lang="en">
  <head>
    
    <!-- Required meta tags -->
    @include('admin.css')
    <style type="text/css">

      .div_center
      {
          text-align: center;
          padding-top: 40px;
      }
      .font_size
      {
        font-size: 40px;
        padding-bottom: 40px;
      }
      .text_color
    {
        color: black;
    }
    .label
    {
      display: inline-block;
      width: 200px;
    }
    .form-group {
    display: flex;
     flex-direction: column;
     gap: 5px;
     }
     .form-group label {
         font-size: 20px;
     color: black;
     font-weight: bold;
     }
     .form-group input,
     .form-group textarea {
         border: 1px solid #ababab;
     border-radius: 5px;
     padding: 5px;
     }
     .div_desgin
     {
      padding-bottom: 15%;
     }
      </style>
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.header')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            @if (session()->has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            {{ session()->get('message') }}
          </div>
          @endif
           <div class="div_center">
            <h1 class="font_size"> Add Product</h1>


            <form action="{{ route('add_product') }}" method="POST" enctype="multipart/form-data">
              @csrf

             <table>
            <div class="div_design">
            <label class="form-group">Product Title</label>
            <input type="text" class="text_color" name="title" placeholder="Write Product Title " required="">
            </div>

            <div>
              <label class="form-group div_design">Product Description</label>
              <input type="text" class="text_color" name="description" placeholder="Write Product Description" required="">
              </div>

              <div>
                <label class="form-group div_design">Product Category</label>
                <select class="text_color" name="category" required="">
                  <option value="" selected="">Add a category here</option>
                  @foreach ($category as $category )
            
                  <option value="{{ $category->category_name  }}">{{ $category->category_name  }}</option>

                  @endforeach
                  
                </select>
              </div>

                <div>
                  <label class="form-group div_design">Product Image</label>
                  <input type="file"  name="image" required="">
                  </div>

                  <div>
                    <label class="form-group div_design">Product Price</label>
                    <input type="number" class="text_color" name="price" placeholder="Write Product Price" required="">
                    </div>

                    <div>
                      <label class="form-group div_design">Product Stock</label>
                      <input type="number" class="text_color" name="stock" placeholder="Write Product Stock" required="">
                      </div>

                    <div>
                      <button value="Add Product" type="submit" class="btn btn-primary" >Add Product</button>
                      </div>
            </form>


           </div>
          </div>
        </div>
      
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
    
  </body>
</html>