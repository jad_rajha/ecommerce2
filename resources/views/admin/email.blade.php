<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <base href="/public">
    @include('admin.css')

    <style>
    label 
    {
        display: inline-block;
        width: 200px;
        font-size: 15px;
        font-weight: bold;
        
    }

    </style>
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.header')
      <div class="main-panel">
        <div class="content-wrapper">
            <h1 style="text-align: center; font-size:25px; "> Send Email to {{ $order->email }}</h1>
            <form action="{{ route('send_user_email', $order->id) }}" method="POST">
                @csrf
            <div style="padding-left: 35%; padding-top: 30px;">
            <label> Greetings </label>
            <input style="color:black;" type="text" name="greeting">
            </div>

            <div style="padding-left: 35%; padding-top: 30px;">
                <label> Subject </label>
                <input style="color:black;" type="text" name="subject">
                </div>

                <div style="padding-left: 35%; padding-top: 30px;">
                    <label> Email Body </label>
                    <input style="color:black;" type="text" name="body">
                    </div>

                    <div style="padding-left: 35%; padding-top: 30px;">
                        <label> Email Button Name </label>
                        <input style="color:black;" type="text" name="button">
                        </div>

                        <div style="padding-left: 35%; padding-top: 30px;">
                            <label> Email URL </label>
                            <input style="color:black;" type="text" name="url">
                            </div>

                            <div style="padding-left: 35%; padding-top: 30px;">
                                <label> Email Last Line </label>
                                <input style="color:black;" type="text" name="lastline">
                                </div>

                            <div style="padding-left: 35%; padding-top: 30px;">
                                 <label> Submit Button</label>
                                 <button type="submit" value="Send Email" class="btn btn-primary">Send Email</button>
                                </div>

                            
            </form> 

        </div>
      </div>

        <!-- partial -->
        
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>