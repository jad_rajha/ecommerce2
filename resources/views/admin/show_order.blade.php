<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    @include('admin.css')
    <style>
        .title_deg
        {
            text-align: center;
            font-size: 15px;
            font-weight: bold;
            padding-bottom: 50px;
            
        }
        .table_deg
        {
            border: 2px solid white;
            width: 200%;
            margin: auto;
            text-align: center;
            background-color: skyblue;
        }
        
                
          
        
        
        
        
    </style>
        
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.header')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <h1 class="title_deg">All Orders</h1>
                <div>
                  <form action="{{ route('search') }}" method="get">
                    @csrf
                    <input type="text" style="color: black" name="Search" placeholder="search for something">
                    <input type="submit" value="Search" class="btn btn-outline-primary">
                    </form>
                <table>
                    <tr class="table_deg">
                        <th class="padding: 10px;">Name</th>
                        <th class="padding: 10px;">Email</th>
                        <th class="padding: 10px;">Phone</th>
                        <th class="padding: 10px;">Address</th>
                        <th class="padding: 10px;">ID</th>
                        <th class="padding: 10px;">Product Title</th>
                        <th class="padding: 10px;">Product Quanitity</th>
                        <th class="padding: 10px;">Product Price</th>
                        <th class="padding: 10px;">Product ID</th>
                        <th class="padding: 10px;">Payment Status</th>
                        <th class="padding: 10px;">Delivery Status</th>
                        <th class="padding: 10px;">Action</th>
                        <th class="padding: 10px;">Delivered</th>
                        <th class="padding: 10px;">Send Email</th>
                        
                        </tr>
                        
                          
                        @foreach ($order as $order)
                        <tr>
                         
                           <td>{{ $order->name }}</td> 
                           <td>{{ $order->email }}</td> 
                           <td>{{ $order->phone }}</td> 
                           <td>{{ $order->address }}</td> 
                           <td>{{ $order->user_id }}</td> 
                           <td>{{ $order->product_title }}</td> 
                           <td>{{ $order->product_quantity }}</td> 
                           <td>{{ $order->product_price }}</td> 
                           <td>{{ $order->product_id }}</td>
                           <td>{{ $order->payment_status }}</td>
                           <td>{{ $order->delivery_status }}</td> 
                           <td><a class="btn btn-danger" href="{{  route ('delete_order',$order->id)  }}">Delete</a></td>
                           <td> 
                            @if($order->delivery_status=='processing')
                            <a class="btn btn-primary " href="{{  route ('delivered',$order->id)  }}">Delivered</a>
                            @else
                            <p style="color: green">Delivered</p>
                            @endif
                          </td>
                          <td>
                            <a href="{{ route('send_email', $order->id) }}" class="btn btn-info">Send Email</a>
                          </td>
                        </tr>
                        
                        </tr>
                        @endforeach
                        
                          
                </table>
            </div>
        </div>

    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>