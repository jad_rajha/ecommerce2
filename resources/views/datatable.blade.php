<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatitable" content="IE=edge">
    <meta name="viewpoint" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <title>Datatable</title>
  </head>
  <body>
    <div class="container mt-5">
      
    <table id="myTable">
      <thead>
        <tr>
          
            <th class="th_deg">Id</th>
            <th class="th_deg">Title</th>
         <th class="th_deg">Description</th>
           <th class="th_deg">Category</th>
            <th class="th_deg">Price</th>
              
        </tr>
        <tbody>
          @foreach ($datatables as $datatables )
          <tr>
        
                    <td>{{ $product->id  }}</td> 
                    <td>{{ $product->title  }}</td>
                    <td>{{ $datatables->description }}</td>
                    <td>{{ $datatables->category }}</td>
                    <td>{{ $datatables->price }}</td>
                    
                    </tr>

            
          @endforeach
        </tbody>
      </thead>
    </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script> let table = new DataTable('#myTable'); </script>
  </body>
</html>
    
