<!DOCTYPE html>
<html>
   <head>
    <base href="/public">
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Site Metas -->
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="images/favicon.png" type="">
      <title>Jad E-commerce Webstore</title>
      <!-- bootstrap core css -->
      <link rel="stylesheet" type="text/css" href="home/css/bootstrap.css" />
      <!-- font awesome style -->
      <link href="home/css/font-awesome.min.css" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="home/css/style.css" rel="stylesheet" />
      <!-- responsive style -->
      <link href="home/css/responsive.css" rel="stylesheet" />
   </head>
   <body>
      <div class="hero_area">
         <!-- header section strats -->
        @include('home.header')
      </div>
      <div class="col-sm-6 col-md-4 col-lg-4" style="margin: auto; width:50%; padding:30px">
       
           <div class="img-box">
              <img src="product/{{ $product->image }}" alt="">
           </div>
           <div class="detail-box">
              <h5>
                 {{ $product->title }}
              </h5>
              <h6>
                Price: S.P {{ $product->price }}
              </h6>
              Description:  <h6> {{ $product->description }}</h6>
             Category: <h6> {{ $product->category }}</h6>
             <form action="{{ route('add_cart', $product->id) }}" method="Post">
               @csrf
               <div>
               <input type="number" name="quantity" value="" min="1">
               <button class="btn btn-primary">Add to Cart</button>
               </div>
          </div>
       </div>
      </form>
           </div>
        </div>
     </div>
         <!-- end header section -->
         <!-- slider section -->
        <div>
      @include('home.footer')
        </div>