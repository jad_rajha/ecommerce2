<!DOCTYPE html>
<html>
   <head>
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Site Metas -->
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="images/favicon.png" type="">
      <title>Jad E-commerce Webstore</title>
      <!-- bootstrap core css -->
      <link rel="stylesheet" type="text/css" href="home/css/bootstrap.css" />
      <!-- font awesome style -->
      <link href="home/css/font-awesome.min.css" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="home/css/style.css" rel="stylesheet" />
      <!-- responsive style -->
      <link href="home/css/responsive.css" rel="stylesheet" />
      <style type="text/css">
      .center
      {
        margin: auto;
        width: 80%;
        text-align: center;
        padding: 30px;

      }
       table,th,td
      {
        border: 1px solid gray;

      }
      .th_deg
      {
        font-size: 30px;
        padding: 5px;
        background-color: skyblue;
      }
      .total_deg
      {
        font-size: 20px;
        padding: 40px;
      }
      </style>
      
   </head>
   <body>
      <div class="hero_area">
         <!-- header section strats -->
        @include('home.header')
         <!-- end header section -->
         <!-- slider section -->
         
         <!-- end slider section -->
         @if (session()->has('message'))
         <div class="alert alert-success">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
           {{ session()->get('message') }}
         </div>
         @endif

      <div class="center">
        <table>
            <tr>
            <th class="th_deg">Product Title</th>
            <th class="th_deg">Product Quantity</th>
            <th class="th_deg">Product Price</th>
            <th class="th_deg">Action</th>
            </tr>
            
           <?php $totalprice=0; ?>

            @foreach ($cart as $cart )
              
            <tr>
              <td>{{ $cart->product_title }}</td>
              <td>{{ $cart->product_quantity }}</td>
              <td>{{ $cart->product_price }}</td>
              <td><a class="btn btn-danger" onclick="retrun confirm('Are you sure to remove the product?')" href="{{ route('remove_cart', $cart->id) }}">Remove from Cart</a></td>
            </tr>
            

            <?php $totalprice=$totalprice + $cart->product_price ?> 
            
            @endforeach

           

        </table>
        <div>
          <h1 class="total_deg">Total Price: {{ $totalprice }}</h1>
        </div>

        <div class="center">
        <h1 style="font-size: 25px; padding-bottom: 15px;"> Press the Button below to Purchase</h1> 
        <a href="{{ route('cash_delivery') }}" class="btn btn-danger"> Cash on Delivery</a>
      </div>  
      </div>
      </div>
      </div>
      

      
      <!-- why section -->
      <div>
      <!-- footer start -->
      @include('home.footer')
      </div>