<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response

    {
        if(Auth::user()){
            if(auth()->user()->usertype == '1'){
                return $next($request);
            }
    
             elseif(auth()->user()->usertype == '0') {
    
                return abort('401');
             }
            
        }
        else{
            return abort('404');
        }
       
    }
}
