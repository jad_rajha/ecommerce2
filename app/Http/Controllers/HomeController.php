<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Order;


class HomeController extends Controller
{
    public function index(){
        $product=Product::paginate(3);
    return view ('home.userpage', compact('product'));
    }

    public function redirect()
    {
        $usertype=Auth::user()->usertype;
        
        if($usertype=='1')
        {
            $total_product=Product::all()->count();
            $total_order=Order::all()->count();
            $total_customer=User::all()->count();
            $order=Order::all();
            $total_revenue=0;
            
            foreach ($order as $order)
            {
                $total_revenue=$total_revenue + $order->product_price;
            }
            $processing_orders=Order::where('delivery_status','=','processing')->get()->count();
            $delivered_orders=Order::where('delivery_status','=','delivered')->get()->count();
            
            return view('admin.home', compact('total_product','total_order','total_customer','total_revenue','delivered_orders','processing_orders'));

        }
        else 
        {
            $product=Product::paginate(3);
            return view('home.userpage', compact('product'));
        }
    }
        public function product_details($id){
            
        $product=Product::find($id);
        return view('home.product_details', compact('product'));

        
        }

        public function add_cart(Request $request, $id){
            if(Auth::id())
            {
                
                $user=Auth::user();
                $product=Product::find($id);
                
                $stock=$product->stock - $request->quantity;
                if($stock>0){
                 
                
                
                Product::where('id',$id)->update([
                    'stock'=>$stock,
                ]);

                $cart = new cart;
             
                $cart->name=$user->name;
                $cart->email=$user->email;
                $cart->phone=$user->phone;
                $cart->address=$user->address;
                $cart->user_id=$user->id;
                $cart->product_title=$product->title;
                $cart->product_stock=$product->stock;
                $cart->product_price=$product->price * $request->quantity;
                $cart->product_stock=$product->stock - $request->quantity;
                $cart->product_id=$product->id;
                $cart->product_quantity=$request->quantity;

                $cart->save();
                return redirect('/redirect');





            }
            elseif($stock<0){
                return redirect()->back()->with('messageq','Sorry, this quantity is not available right now');
            }
        }
            else
            
            return redirect('login');

        
        

        }


       

        public function show_cart() {
            if(Auth::id()) {
            
            $id=Auth::user()->id;
            $cart=Cart::where('user_id','=',$id)->get();
            return view('home.showcart', compact('cart'));
        }

        else {
            return redirect('login');
        }              
        
}
        public function remove_cart($id){
        $cart=Cart::find($id);
        $cart->delete();
        return redirect()->back();
        }

         


         


        public function cash_delivery(){
        $user=Auth::user();
        $userid=$user->id;
        $data=Cart::where('user_id','=',$userid)->get();
        
        foreach ($data as $data)
        {
            $product=Product::all();
            $order= new Order;
            $order->name=$data->name;
            $order->email=$data->email;
            $order->phone=$data->phone;
            $order->address=$data->address;
            $order->user_id=$data->user_id;
            $order->product_title=$data->product_title;
            $order->product_price=$data->product_price;
            $order->product_quantity=$data->product_quantity;
            $order->product_id=$data->product_id;
            $order->payment_status='cash_on_delivery';
            $order->delivery_status='processing';
            $order->save();
           # code...
        }
        

        return redirect()->back()->with('message', 'Thank you for your order, we will contact you as soon as possible');

        }
}
