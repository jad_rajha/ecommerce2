<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Datatable;
use App\Models\Order;
use App\Models\Product;
use App\Models\Cart;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Notification as FacadesNotification;
use App\Notifications\MyNotification;


class AdminController extends Controller
{
    public function view_category()
    {
        $data=category::all();

        return view('admin.category', compact('data'));
    }

    public function add_category(Request $request)
    {
        
        $data = new category;
        $data->category_name=$request->category;
        $data-> save();
        return redirect()->back()->with('message', 'Category Added Successfully');
    }

    public function delete_category($id){
         $data=category::find($id);
         $data->delete();
         return redirect()->back()->with('message', 'Category Deleted Successfully');
    
    }
    public function view_product()
    { 
        $category=Category::all();

        return view('admin.product', compact('category'));
    }

    public function add_product(Request $request)
    {
        
        $product = new Product();
        $image=$request->image;
        $imagename=time().'.'. $image->getClientOriginalExtension();
        $request->image->move('product', $imagename);
        $product->image=$imagename;
        $product->title=$request->title;
        $product->description=$request->description;
        $product->category=$request->category;
        $product->price=$request->price;
        $product->stock=$request->stock;

        $product-> save();
        return redirect()->back()->with('message', 'Product Added Successfully');
    }

    public function show_product()
    {
        $product=Product::all();

        return view('admin.show_product', compact('product'));
    }
    
    public function delete_product($id){
        $product=product::find($id);
        $product->delete();
        return redirect()->back()->with('message', 'Product Deleted Successfully');
   
   }
   public function update_product($id){
    $product=product::find($id);
    $category=Category::all();
    return view ('admin.update_product', compact('category', 'product'));
   }


   public function update_product_confirm(Request $request,$id){
    $product=Product::find($id);
    $product->title=$request->title;
    $product->description=$request->description;;
    $product->category=$request->category;
    $product->price=$request->price;
    $product->stock=$request->stock;
    $image=$request->image;
    $imagename=time().'.'. $image->getClientOriginalExtension();
    $request->image->move('product', $imagename);
    $product->image=$imagename;
    $product-> save();
    return redirect('show_product')->with('message', 'Product Updated Successfully');
    
   }
   public function show_order(){
    $order=Order::all();
    return view('admin.show_order', compact('order'));

   }
   public function delete_order($id){
    $order=Order::find($id);
    $order->delete();
    return redirect()->back();

}
 public function delivered($id){
    $order=Order::find($id);
    $order->delivery_status="delivered";
    $order->save();
    return redirect()->back();
 }
 public function search(Request $request){
    $searchText=$request->Search;
    $order=Order::where('name', 'LIKE', "%searchText%")->OrWhere('phone', 'LIKE', "%searchText%")->
    OrWhere('email', 'LIKE', "%searchText%")->OrWhere('address', 'LIKE', "%searchText%")->OrWhere('product_title', 'LIKE', "%searchText%")
    ->OrWhere('product_quantity', 'LIKE', "%searchText%")->OrWhere('product_price', 'LIKE', "%searchText%")
    ->OrWhere('product_id', 'LIKE', "%searchText%")->OrWhere('payment_status', 'LIKE', "%searchText%")
    ->OrWhere('delivery_status', 'LIKE', "%searchText%")->get();
    
    return view('admin.show_order', compact('order'));
 }

 public function send_email($id){
    $order = Order::find($id);
 return view('admin.email', compact('order'));
 }

 public function send_user_email(Request $request, $id){
    $order = Order::find($id);
    $details = [ 
        'greeting'=> $request->greeting,
        'subject'=> $request->subject,
        'body'=> $request->body,
        'button'=> $request->button,
        'url'=>$request->url,
        'lastline'=>$request->lastline,
    ];

         FacadesNotification::send($order, new MyNotification($details));
         return redirect()->back();
 }

}