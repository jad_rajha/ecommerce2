<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\DatatableController;
use App\Http\Controllers\DashboardController;
use App\Models\Category;
use App\Models\Datatable;
use App\Models\Product;
use App\Models\Order;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('/redirect', [HomeController::class, 'redirect'])->name('/redirect');
Route::get('view_category', [AdminController::class, 'view_category'])->name('view_category')->middleware('admin');
Route::post('add_category', [AdminController::class, 'add_category'])->name('add_category')->middleware('admin');
Route::get('delete_category/{id}', [AdminController::class, 'delete_category'])->name('delete_category')->middleware('admin');
Route::get('view_product', [AdminController::class, 'view_product'])->name('view_product')->middleware('admin');
Route::post('add_product', [AdminController::class, 'add_product'])->name('add_product')->middleware('admin');
Route::get('show_product', [AdminController::class, 'show_product'])->name('show_product')->middleware('admin');
Route::get('delete_product/{id}', [AdminController::class, 'delete_product'])->name('delete_product')->middleware('admin');
Route::get('update_product/{id}', [AdminController::class, 'update_product'])->name('update_product')->middleware('admin');
Route::post('update_product_confirm/{id}', [AdminController::class, 'update_product_confirm'])->name('update_product_confirm')->middleware('admin');
Route::get('show_order', [AdminController::class, 'show_order'])->name('show_order')->middleware('admin');
Route::get('delete_order/{id}', [AdminController::class, 'delete_order'])->name('delete_order')->middleware('admin');
Route::get('delivered/{id}', [AdminController::class, 'delivered'])->name('delivered')->middleware('admin');
Route::get('search', [AdminController::class, 'search'])->name('search')->middleware('admin');
Route::get('send_email/{id}', [AdminController::class, 'send_email'])->name('send_email')->middleware('admin');
Route::get('send_user_id/{id}', [AdminController::class, 'send_user_id'])->name('send_user_id')->middleware('admin');
Route::get('send_user_email/{id}', [AdminController::class, 'send_user_email'])->name('send_user_email')->middleware('admin');




Route::get('data_table', [DatatableController::class, 'index'])->name('data_table')->middleware('admin');
Route::get('delete_table/{id}', [DatatableController::class, 'delete_table'])->name('delete_table')->middleware('admin');

Route::get('product_details/{id}', [HomeController::class, 'product_details'])->name('product_details');
Route::post('add_cart/{id}', [HomeController::class, 'add_cart'])->name('add_cart');
Route::get('show_cart', [HomeController::class, 'show_cart'])->name('show_cart');
Route::get('remove_cart/{id}', [HomeController::class, 'remove_cart'])->name('remove_cart');
Route::get('cash_delivery', [HomeController::class, 'cash_delivery'])->name('cash_delivery');








       
       






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
